//
//  thirdVCViewController.swift
//  FirstProject
//
//  Created by Florent Frossard on 15/12/2019.
//  Copyright © 2019 Florent Frossard. All rights reserved.
//

import UIKit

class thirdVCViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
 
    @IBOutlet weak var tableView: UITableView!
    
    let nameChapeaux = ["Bonnet","Béret","Casquette","Cloche","Feutre"]
      
    let imagesChapeaux = [UIImage(named: "Bonnet"), UIImage(named: "Beret"),UIImage(named: "Casquette"),UIImage(named: "Cloche"),UIImage(named: "Feutre")]
      
    let heartShappedButton = UIImage(named: "suit.heart")

    override func viewDidLoad() {
        super.viewDidLoad()

       
        tableView.delegate   = self
        tableView.dataSource = self
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameChapeaux.count
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        cell.imageView?.image = imagesChapeaux[indexPath.row]
        cell.textLabel?.text = nameChapeaux[indexPath.row]
   
        
        return cell
        
}

}
