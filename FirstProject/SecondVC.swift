//
//  SecondVC.swift
//  FirstProject
//
//  Created by Florent Frossard on 12/12/2019.
//  Copyright © 2019 Florent Frossard. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class SecondVC: UIViewController,ARSCNViewDelegate {
    
  

    @IBOutlet var sceneView: ARSCNView!
    
   
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Set the view's delegate
            sceneView.delegate = self
            
            // Show statistics such as fps and timing information
            sceneView.showsStatistics = true
            
            // Create a new scene
//            let scene = SCNScene(named: "")!
            
//            sceneView.scene = scene
            
//            
//           let image = UIImage(named: "Casquette")
//           let node = SCNNode(geometry: SCNPlane(width: 1, height: 1))
//           node.geometry?.firstMaterial?.diffuse.contents = image
            
            
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
            // Create a session configuration
            let configuration = ARFaceTrackingConfiguration()

            // Run the view's session
            sceneView.session.run(configuration)
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            
            // Pause the view's session
            sceneView.session.pause()
        }

        // MARK: - ARSCNViewDelegate
        
    /*
        // Override to create and configure nodes for anchors added to the view's session.
        func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
            let node = SCNNode()
         
            return node
        }
    */
        
        func session(_ session: ARSession, didFailWithError error: Error) {
            // Present an error message to the user
            print("error")
        }
        
        func sessionWasInterrupted(_ session: ARSession) {
            // Inform the user that the session has been interrupted, for example, by presenting an overlay
            
        }
        
        func sessionInterruptionEnded(_ session: ARSession) {
            // Reset tracking and/or remove existing anchors if consistent tracking is required
            
        }
    }
