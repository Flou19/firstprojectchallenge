//
//  CollectionViewCell.swift
//  FirstProject
//
//  Created by Florent Frossard on 12/12/2019.
//  Copyright © 2019 Florent Frossard. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var heartButton: UIButton!
    
       
    var isDouble = true
    
    
    
    @IBAction func heartBUtton(_ sender: Any) {
       
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(changeHeart))
        
            heartButton.isUserInteractionEnabled = true
            heartButton.addGestureRecognizer(gestureRecognizer)

        }
    
    @objc func changeHeart() {
        
    
        
        if isDouble == true {
            
            heartButton.imageView?.image = UIImage(systemName: "suit.heart")
            heartButton.tintColor = .opaqueSeparator
            isDouble = false
            
        } else {
            
            heartButton.imageView?.image = UIImage(systemName: "suit.heart.fill")
            heartButton.tintColor = .red
            isDouble = true
            
        }
    }
}

