//
//  ViewController.swift
//  FirstProject
//
//  Created by Florent Frossard on 12/12/2019.
//  Copyright © 2019 Florent Frossard. All rights reserved.
//

import UIKit


class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
     @IBOutlet weak var favButton: UIButton!
    
    let nameChapeaux = ["Bonnet","Béret","Casquette","Cloche","Feutre"]
    
    let imagesChapeaux = [UIImage(named: "Bonnet"), UIImage(named: "Beret"),UIImage(named: "Casquette"),UIImage(named: "Cloche"),UIImage(named: "Feutre")]
    
    let heartShappedButton = UIImage(named: "suit.heart")
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate     = self
        collectionView.dataSource   = self
  }
 
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return nameChapeaux.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        
            cell.imageView.image = imagesChapeaux[indexPath.item]
            cell.nameLabel.text  = nameChapeaux[indexPath.item]
     
     
            cell.backgroundColor = .white
            cell.layer.cornerRadius = 12
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 3, height: 3.0)
            cell.layer.shadowRadius = 1.0
            cell.layer.shadowOpacity = 0.1
            cell.layer.masksToBounds = false
       
        return cell
    }
    

//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        performSegue(withIdentifier: "cellToArKit", sender: nil)
//    }
//
//    @IBAction func ArkitButtonAction(_ sender: Any) {
//        
//        performSegue(withIdentifier: "toArKit", sender: nil)
//      
//    }
//    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
//        if segue.identifier == "toArKit" {
//            
//            _ = segue.destination
//            
//            print("there")
//        }
//    }
}

